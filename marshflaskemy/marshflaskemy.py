from flask import Flask, request
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from marshmallow import fields
from sqlalchemy import Table, Column, Integer, String, ForeignKey
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import relationship
from werkzeug.exceptions import BadRequest

class Config(object):
    SQLALCHEMY_DATABASE_URI = '<CONNECTION STRING HERE>'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
ma = Marshmallow(app)

# Model
#-----------------------------------------------------------------------------------------------------------------------

class MailAddress(db.Model):
    __tablename__ = 'mail_addresses'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    
    mail_type = Column(String(200), nullable=False)
    mail = Column(String(200), nullable=False)

    def __init__(self, mail, mail_type):
        self.mail = mail
        self.mail_type = mail_type
    
class MailAddressSchema(ma.ModelSchema):
    class Meta:
        model = MailAddress

class User(db.Model):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String(200), nullable=False)
    mail_addresses = relationship('MailAddress', backref='user')

    def __init__(self, name, mail_addresses):
        self.name = name
        self.mail_addresses = mail_addresses

    def __hash__(self):
        return hash(self.name)

class UserSchema(ma.ModelSchema):
    mail_addresses = fields.Nested(MailAddressSchema, many = True, only=('mail', 'mail_type'))
    class Meta:
        model = User

user_schema = UserSchema()

# Routes
#-----------------------------------------------------------------------------------------------------------------------
@app.route('/api/v0/user', methods=['GET'])
def user_get():
    users = db.session.query(User).all()
    return user_schema.jsonify(users, many = True), 200

@app.route('/api/v0/user', methods=['POST'])
def user_create():
    if request.json is None:
        raise BadRequest('json payload required')
    try:
        new_instance = user_schema.make_instance(request.json)
        db.session.add(new_instance)
        db.session.commit()
        return user_schema.jsonify(new_instance, many = False), 201
    except IntegrityError as err:
        raise BadRequest(err)

# main
#-----------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    app.run('localhost', 5555)
    users = db.session.query(User).all()
    u_schema = UserSchema()
    for u in users:
        print (u_schema.dump(u).data)
        
